import 'dart:async';
import 'dart:convert';
import 'package:example_flutter/Utils/constant.dart';
import 'package:example_flutter/models/user.dart';
import 'package:http/http.dart' as http;

class UserBloC {
  StreamController<List<User>> _controller = StreamController<List<User>>();
  Sink<List<User>> get _inputUser => _controller.sink;
  Stream<List<User>> get outputUser => _controller.stream;
  static List<User> userList;

  dispose() {
    _controller.close();
  }

  UserBloC() {
    getUserList();
  }

  getUserList() {
    http.get(Api.getAll).then((response) {
      Map<String, dynamic> json = jsonDecode(response.body);
      List<dynamic> usersJson = json['data'];

      List<User> userList =
          usersJson.map((userJson) => User.fromJson(userJson)).toList();
      UserBloC.userList = userList;
      _inputUser.add(userList);
    });
  }

  increaseCount(User user) {
    var body = user.increasedCountJson();
    print('Json: $body');
    http.put(Api.baseUrl + '${user.id}', body: body).then((response) {
      UserBloC.userList.firstWhere((usr) => usr.id == user.id).count += 1;
      _inputUser.add(UserBloC.userList);
    });
  }

  deleteUser(User user) {
    http.delete(Api.baseUrl + '${user.id}').then((response) {
      UserBloC.userList.remove(user);
      _inputUser.add(UserBloC.userList);
    });
  }

  addNewUser() {
    http.post(Api.baseUrl + 'save', body: User.randomUser()).then((response) {
      getUserList();
    });
  }
}
